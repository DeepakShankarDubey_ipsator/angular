import { getMenuItemKey, MenuItemsState } from '@components/menu/MenuItemsReducer';
import { ChangeQuantityCb } from '@hooks/useMenu';
import { log } from 'console';
import React, { ReactNode } from 'react'
import { CategoryItemListProps } from './CategoryItemListMobile';
import { MenuItem } from './MenuItem';
import { CategoryItemListInfo, MenuItemInfo } from './types';


type UnExpandableCategoryItemListProps = {
    itemsSelectionInfo: MenuItemsState["itemsSelectionInfo"];
    changeQuantity: ChangeQuantityCb;
    categories: any[];
    checkedCategories: any[];
    renderMenuItems;
    name: string;
    viewType: string;
    items;
    categoryKey;
    i;
    noOfItems

};


const UnExpandableCategory: React.FC<UnExpandableCategoryItemListProps> = ({
    categories,
    itemsSelectionInfo,
    changeQuantity,
    checkedCategories,
    renderMenuItems,
    name, viewType, items, categoryKey,i,noOfItems

}) => {


    




     if (checkedCategories.find(e => e?.name === name)) {
            return (
              <section className={`${i !== 0 ? "pt-0 mt-0" : ""} pb-3 px-6`} id={name} key={name}>
  
                {renderMenuItems(name, items)}
              </section>
            )
          }

          else{
            console.log("in else--", );
            
            // checkedCategories.push(category)
            return (
              <section className={`${i !== 0 ? "pt-12 mt-12" : ""} pb-3 px-6`} id={name} key={name}>
  
  
  
                <h1 className="text-4.5xl text-gray-70">{name}</h1>
                <p className="text-sm text-gray-60">
  
                  {noOfItems} item{noOfItems > 1 ? "s" : ""}
                </p>
  
                {renderMenuItems(name, items)}
              </section>
            );

          }

}

export default UnExpandableCategory