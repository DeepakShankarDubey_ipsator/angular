import React, { ReactNode } from "react";
import { Accordion } from "@components/Accordion";
import { ChangeQuantityCb } from "@hooks/useMenu";
import { MenuItem } from "./MenuItem";
import { getMenuItemKey, MenuItemsState } from "@components/menu/MenuItemsReducer";
import { CategoryItemListInfo, MenuItemInfo } from "./types";
import { joinNamesArray } from "@services/utils/Common";

export type CategoryItemListProps = {
  itemsSelectionInfo: MenuItemsState["itemsSelectionInfo"];
  changeQuantity: ChangeQuantityCb;
  categories: CategoryItemListInfo[];
  handleSetCategoryExpandedState: (categoryKey: string, expanded: boolean) => void;
  categoryExpandedState: Record<string, boolean>;
  MenuData: any;
  currentOutletInfo: any;
  children?: ReactNode;
};

export const CategoryItemListMobile: React.FC<CategoryItemListProps> = ({
  categories,
  itemsSelectionInfo,
  changeQuantity,
  handleSetCategoryExpandedState,
  categoryExpandedState,
}) => {
  // debugger
  const renderMenuItems = (categoryName: string, viewType: CategoryItemListInfo["viewType"], menuItems: MenuItemInfo[]) =>
    menuItems.map((item, index) => {

      console.log("categoryName--", categoryName, '\n', "viewType--", viewType);

      let checkedCategories = []


      const menuItemKey = getMenuItemKey(categoryName, item.id);
      return (
        <MenuItem
          key={item.id + index + item.itemName}
          {...item}
          viewType={viewType}
          itemSelectionInfo={itemsSelectionInfo[menuItemKey]}
          changeQuantity={(type) => changeQuantity(type, item, menuItemKey, null)}
          category={categoryName}
        />
      );
    });

  return (
    <>
      {categories.map((category, i) => {
        const { name, viewType, items, showExpanded } = category;
        const categoryKey = name + "_" + i;

        return (
          <div className={`bg-white ${!!i ? "mt-2" : ""} py-5 px-4`} key={name } >

            {viewType === "EXPANDABLE" ? (
              <Accordion
                header={(isExpanded) => (
                  <div className="w-10/12">
                    <p className="subtitle-1 mb-1">{name}*//***expand***dsdhkjhj</p>
                    <p className={`body-3 text-gray-60 transition-opacity ${isExpanded ? "opacity-0" : "opacity-100"}`}>
                      {joinNamesArray(items, "itemName")}
                    </p>
                  </div>
                )}
                initialExpand={categoryExpandedState[categoryKey] ?? showExpanded}
                headerClassName="justify-between"
                childrenClassName="-mt-6"
                onExpandChange={(expanded) => handleSetCategoryExpandedState(categoryKey, expanded)}
                categoryName={name}
                enableAnalytics={true}
              >
                {renderMenuItems(name, viewType, items)}
              </Accordion>
            ) : (
              <>

 
     if (checkedCategories.find(e => e?.name === category.name)) {
            return (
                <div className={viewType === "GRID" ? `flex flex-wrap justify-between gap-y-8` : ""}>{renderMenuItems(name, viewType, items)}</div>
                )
        }

                else {
                  console.log("in else--",)

            checkedCategories.push(category)

                return (

                <p className="subtitle-1 mb-1">{name}***not__expand***</p>

                <div className={viewType === "GRID" ? `flex flex-wrap justify-between gap-y-8` : ""}>{renderMenuItems(name, viewType, items)}</div>
                )

        }



                {/* </> */}



                {/* <p className="subtitle-1 mb-1">{name}***not__expand***</p>

                <div className={viewType === "GRID" ? `flex flex-wrap justify-between gap-y-8` : ""}>{renderMenuItems(name, viewType, items)}</div> */}

              
              </>
            )}
          </div>
        );
      })}
    </>
  );
};
