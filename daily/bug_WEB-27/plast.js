import React, { ReactNode } from "react";
import { Accordion } from "@components/Accordion";
import { ChangeQuantityCb } from "@hooks/useMenu";
import { MenuItem } from "./MenuItem";
import { getMenuItemKey, MenuItemsState } from "@components/menu/MenuItemsReducer";
import { CategoryItemListInfo, MenuItemInfo } from "./types";
import { joinNamesArray } from "@services/utils/Common";
import { log } from "console";

export type CategoryItemListProps = {
  itemsSelectionInfo: MenuItemsState["itemsSelectionInfo"];
  changeQuantity: ChangeQuantityCb;
  categories: CategoryItemListInfo[];
  handleSetCategoryExpandedState: (categoryKey: string, expanded: boolean) => void;
  categoryExpandedState: Record<string, boolean>;
  MenuData: any;
  currentOutletInfo: any;
  children?: ReactNode;
};

export const CategoryItemListMobile: React.FC<CategoryItemListProps> = ({
  categories,
  itemsSelectionInfo,
  changeQuantity,
  handleSetCategoryExpandedState,
  categoryExpandedState,
}) => {
  // debugger
  const renderMenuItems = (categoryName: string, viewType: CategoryItemListInfo["viewType"], menuItems: MenuItemInfo[]) =>
    menuItems.map((item, index) => {

      console.log("categoryName--", categoryName, '\n', "viewType--", viewType);
      
     
      


      const menuItemKey = getMenuItemKey(categoryName, item.id);
      return (
        <MenuItem
          key={item.id + index + item.itemName}
          {...item}
          viewType={viewType}
          itemSelectionInfo={itemsSelectionInfo[menuItemKey]}
          changeQuantity={(type) => changeQuantity(type, item, menuItemKey, null)}
          category={categoryName}
        />
      );
    });

  return (
    <>
      {categories.map((category, i) => {
        const { name, viewType, items, showExpanded } = category;
        const categoryKey = name + "_" + i;

        if (checkedCategories.find(e => e?.name === category.name)) {
          return (
            <>
              <div className={viewType === "GRID" ? `flex flex-wrap justify-between gap-y-8` : ""}>{renderMenuItems(name, viewType, items)}</div>
              </>
              )
      }

              else {
                console.log("in else--",)

          checkedCategories.push(category)

              return (
<>
              <p className="subtitle-1 mb-1">{name}***not__expand***</p>

              <div className={viewType === "GRID" ? `flex flex-wrap justify-between gap-y-8` : ""}>{renderMenuItems(name, viewType, items)}</div>
              </>
              )

      }


  
      })}
    </>
  );
};
