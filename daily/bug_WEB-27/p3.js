

//  const noDuplicateCategories = categories.reduce((newArr, category) => {
//     const { name, items } = category;
//     const categoryIndex = newArr.findIndex((category) => category.name === name);
//     if (categoryIndex === -1) {
//       newArr.push(category);
//     } else {
//       newArr[categoryIndex].items.push(...items);
//     }

//     return newArr;
//   }, []);


//     // const calculation = useMemo(() => expensiveCalculation(count), [count]);






import React, { useEffect, useRef, useState } from "react";
import { getMenuItemKey } from "@components/menu/MenuItemsReducer";
import { MenuItemInfo } from "./types";
import { MenuItemDesktop } from "./MenuItemDesktop";
import { CategoryItemListProps } from "./CategoryItemListMobile";
import { getPageContainer, hasScrollBottomReached, hasScrollTopReached } from "@services/utils/Common";
import { IconSearch } from "@assets/icons";
import useMenuSearch from "@hooks/useMenuSearch";
import { useConfig } from "@services/globalConfig";
import MenuSearchDesktop from "./MenuSearchDesktop";

export const CategoryItemListDesktop: React.FC<CategoryItemListProps> = ({
  categories,
  itemsSelectionInfo,
  MenuData,
  currentOutletInfo,
  changeQuantity,
  children,
}) => {
  const {
    handleMenuSearchToggle,
    showMenuSearchModal,
    ShowSuggestions,
    searchRecommendation,
    SetsearchRecommendation,
    searchKeyword,
    setSerchKeyword,
  } = useMenuSearch(MenuData);
  const categoriesBoxRef = useRef<HTMLDivElement>(null);
  const disableScrollListener = useRef(false);

  const noDuplicateCategories = categories.reduce((newArr, category) => {
    const { name, items } = category;
    const categoryIndex = newArr.findIndex((category) => category.name === name);
    if (categoryIndex === -1) {
      newArr.push(category);
    } else {
      newArr[categoryIndex].items.push(...items);
    }

    return newArr;
  }, []);

  const renderMenuItems = (categoryName: string, menuItems: MenuItemInfo[]) =>
    menuItems.map((item, index) => {
      const menuItemKey = getMenuItemKey(categoryName, item.id);
      return (
        <MenuItemDesktop
          key={item.id + index}
          {...item}
          itemSelectionInfo={itemsSelectionInfo[menuItemKey]}
          changeQuantity={(type) => changeQuantity(type, item, menuItemKey, null)}
          category={""}
        />
      );
    });

  const [activeCategory, setActiveCategory] = useState(categories.length > 0 ? categories[0].name : "");
  const globalCommonConfig = useConfig();
  const menu_search_button_above_jumplist = globalCommonConfig?.["menu_search_button_above_jumplist"];

  useEffect(() => {
    const handlePageScroll = (e: any) => {
      if (!disableScrollListener.current) {
        const sections = categoriesBoxRef.current?.getElementsByTagName("section");
        if (sections) {
          const sectionsList = Array.from(sections);
          let activeCategoryId = "";
          if (hasScrollTopReached(e.target)) {
            activeCategoryId = sectionsList[0]?.id;
          } else if (hasScrollBottomReached(e.target)) {
            activeCategoryId = sectionsList[sectionsList.length - 1]?.id;
          } else {
            sectionsList.forEach((section, i) => {
              const { top } = section.getBoundingClientRect();
              if (top < 100 && top > -100) {
                activeCategoryId = section.id;
              }
            });
          }
          activeCategoryId && setActiveCategory(activeCategoryId.replace("#", ""));
        }
      }
    };

    const pageContainerRef = getPageContainer();
    pageContainerRef?.addEventListener("scroll", handlePageScroll);

    return () => {
      pageContainerRef?.removeEventListener("scroll", handlePageScroll);
    };
  }, [categories]);

  const handleScrollCategoryIntoView = (categoryName: string, isfirstItem: boolean) => {
    const selectedCategory = document.getElementById(categoryName);
    if (selectedCategory) {
      const pageContainerRef = getPageContainer();
      disableScrollListener.current = true;
      const dim = selectedCategory.getBoundingClientRect();
      pageContainerRef?.scrollBy({ top: dim.top - (isfirstItem ? 75 : 55) });
      setActiveCategory(categoryName);
      setTimeout(() => (disableScrollListener.current = false), 100);
    }
  };
  return (
    <div className="flex items-start flex-wrap w-screen justify-center lg:max-w-screen-xl mx-auto relative">
      <nav className="px-6 text-right sticky top-14 left-0 w-64 pt-6">
        {menu_search_button_above_jumplist && (
          <div className="right-10 inline-flex mb-4">
            <div
              className="h-9 bg-brand-primary cursor-pointer bg-opacity-30 px-3 rounded leading-3 flex items-center justify-between text-sm font-medium"
              onClick={handleMenuSearchToggle}
            >
              <IconSearch className="h-5 w-5 mr-1.5 text-gray-70" />
              <a className=" ">SEARCH</a>
            </div>
          </div>
        )}
        <ul>
          {noDuplicateCategories.map((category, i) => (
            <li
              className={`text-lg mb-4 cursor-pointer clamp-1 ${activeCategory === category.name ? "text-lg font-medium text-info-highlight" : ""}`}
              key={category.name}
              onClick={() => handleScrollCategoryIntoView(category.name, i === 0)}
            >
              {category.name}
            </li>
          ))}
        </ul>
      </nav>

      <div className="border-l-2 border-gray-100 flex-1 pb-16 mt-6 overflow-y-auto" ref={categoriesBoxRef}>
        {noDuplicateCategories.map((category, i) => {
          const noOfItems = category.items.length;
          return (
            <section className={`${i !== 0 ? "pt-12 mt-12" : ""} pb-3 px-6`} id={category.name} key={category.name}>
              <h1 className="text-4.5xl text-gray-70">{category.name}</h1>
              <p className="text-sm text-gray-60">
                {noOfItems} item{noOfItems > 1 ? "s" : ""}
              </p>
              {renderMenuItems(category.name, category.items)}
            </section>
          );
        })}
      </div>
      <div className="sticky top-14">{children}</div>
      <div className="flex justify-center">
        <MenuSearchDesktop
          {...{
            currentOutletInfo,
            handleMenuSearchToggle,
            showMenuSearchModal,
            ShowSuggestions,
            searchRecommendation,
            SetsearchRecommendation,
            searchKeyword,
            setSerchKeyword,
            itemsSelectionInfo,
            changeQuantity,
          }}
        />
      </div>
    </div>
  );
};
