// let ar=[]

// console.log(ar);

let categories = [
    { name: "Recommended", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["pizza"]},

    { name: "Breakfast", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["pizza"]},

    { name: "Recommended", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["roti, dal"]},

    { name: "combo", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["pizza"]},

    { name: "Breakfast", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["dosa, idli"]},
 ];



 let newData = {}
//  debugger
 for(let i = 0; i < categories.length; i++){
     if(newData.hasOwnProperty(categories[i].name)){
         let key = categories[i].name
         let prev = newData[key].items;
         let curr = categories[i].items;
         let newItems = [...prev, ...curr]
         newData[key].items = newItems
     }else{
         let key = categories[i].name
         newData[key] = categories[i]
     }
 }
 console.log("newData--", newData);
 console.log("Object.values(newData)---", Object.values(newData))


// debugger
//  const noDuplicateCategories = categories.reduce((newArr, item) => {
//         const {name, items} = item;
//       const itemIndex = newArr.findIndex(item => item.name === name)
//       if(itemIndex === -1){
//         newArr.push(item);
//       } else {
//         newArr[itemIndex].items.push(...items);
//       }
      
//       return newArr;
//     }, []);
    
//     console.table("noDuplicateCategories--", noDuplicateCategories)


const noDuplicateCategories = categories.reduce((newArr, category) => {
        const {name, items} = category;
      const categoryIndex = newArr.findIndex(category => category.name === name)
      if(categoryIndex === -1){
        newArr.push(category);
      } else {
        newArr[categoryIndex].items.push(...items);
      }
      
      return newArr;
      }, []);
        
      console.log("noDuplicateCategories--", noDuplicateCategories);



//  let merged_categories = [
//         { name: "Recommended", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["pizza","roti, dal"]},

//         { name: "Breakfast", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["pizza","dosa, idli"]},

//         { name: "combo", showExpanded: false, foodType: "THALI", viewType: "LINEAR", items: ["pizza"]},

//      ];



//   categories.











//      ...categories...categories.




// { id: 6321211, itemName: "Dal fry", description: "200g", basePrice: 100, sellingPrice: 105, taxRate: 5,… }

// { id: 6321181, itemName: "Yellow Dal Tadka", description: "250g", basePrice: 100, sellingPrice: 105,… }

// { id: 6321162, itemName: "Veg Maharaja Thali",… }

//         :
// { id: 6321227, itemName: "Rajma Chawal", description: "Rice 100g, Rajma 150g", basePrice: 150,… }]}
// foodType
// :
// "THALI"




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




// var arr = [{x:1},{x:2},{x:4}];

// arr.reduce(function (a, b) {
//   return {x: a.x + b.x}; // returns object with property x
// })

// // ES6
//  let reduce= arr.reduce((a, b) => ({x: a.x + b.x}));

// // -> {x: 7}

// console.log(arr);
// console.log(reduce);