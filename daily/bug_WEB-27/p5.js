<ul>
{noDuplicateCategories.map((category, i) => (
  <li
    className={`text-lg mb-4 cursor-pointer clamp-1 ${activeCategory === category.name ? "text-lg font-medium text-info-highlight" : ""}`}
    key={category.name}
    onClick={() => handleScrollCategoryIntoView(category.name, i === 0)}
  >
    {category.name}
  </li>
))}
</ul>