import { Accordion } from "@components/Accordion";
import { getMenuItemKey, MenuItemsState } from "@components/menu/MenuItemsReducer";
import { ChangeQuantityCb } from "@hooks/useMenu";
import { joinNamesArray } from "@services/utils/Common";
import React, { ReactNode } from "react";
import { CategoryItemListProps } from "./CategoryItemListMobile";
import { MenuItem } from "./MenuItem";
import { CategoryItemListInfo, MenuItemInfo } from "./types";

type ExpandableCategoryItemListProps = {
  itemsSelectionInfo: MenuItemsState["itemsSelectionInfo"];
  changeQuantity: ChangeQuantityCb;
  handleSetCategoryExpandedState: (categoryKey: string, expanded: boolean) => void;
  categoryExpandedState: Record<string, boolean>;
  categories: any[];

  checkedCategories: any[];
  renderMenuItems;
  name: string;
  viewType: string;
  items;
  categoryKey;
  showExpanded;
};

const ExpandableCategory: React.FC<ExpandableCategoryItemListProps> = ({
  categories,
  itemsSelectionInfo,
  changeQuantity,
  checkedCategories,
  handleSetCategoryExpandedState,
  categoryExpandedState,
  renderMenuItems,
  name,
  viewType,
  items,
  categoryKey,
  showExpanded,
}) => {
  return (
    <div>

      <Accordion
        header={(isExpanded) => (
          <div className="w-10/12">
            <p className="subtitle-1 mb-1">{name}*//***expand***dsdhkjhj</p>
            <p className={`body-3 text-gray-60 transition-opacity ${isExpanded ? "opacity-0" : "opacity-100"}`}>
              {joinNamesArray(items, "itemName")}
            </p>
          </div>
        )}
        initialExpand={categoryExpandedState[categoryKey] ?? showExpanded}
        headerClassName="justify-between"
        childrenClassName="-mt-6"
        onExpandChange={(expanded) => handleSetCategoryExpandedState(categoryKey, expanded)}
        categoryName={name}
        enableAnalytics={true}
      >
        {renderMenuItems(name, viewType, items)}
      </Accordion>
    </div>
  );
};

export default ExpandableCategory;
