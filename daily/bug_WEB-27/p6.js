<div className="border-l-2 border-gray-100 flex-1 pb-16 mt-6 overflow-y-auto" ref={categoriesBoxRef}>




    {categories.map((category, i) => {
        const noOfItems = category.items.length;
        console.log(" category.name--", category.name);
        console.log(" category.viewType--", category.viewType);
        console.log(" category.items--", category.items);
        console.log("noOfItems--", noOfItems);

        // let checkedCategories=[]
        console.log("checkedCategories--", checkedCategories);

        if (checkedCategories.find(e => e?.name === category.name)) {
            return (
                <section className={`${i !== 0 ? "pt-12 mt-12" : ""} pb-3 px-6`} id={category.name} key={category.name}>

                    {renderMenuItems(category.name, category.items)}
                </section>
            )
        }

        else {
            console.log("in else--",);

            checkedCategories.push(category)
            
            return (
                <section className={`${i !== 0 ? "pt-12 mt-12" : ""} pb-3 px-6`} id={category.name} key={category.name}>



                    <h1 className="text-4.5xl text-gray-70">{category.name}</h1>
                    <p className="text-sm text-gray-60">

                        {noOfItems} item{noOfItems > 1 ? "s" : ""}
                    </p>

                    {renderMenuItems(category.name, category.items)}
                </section>
            );

        }


    })}




</div>