import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Todo } from 'src/app/Todo';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Input() "todo": Todo;
  // @Input() todo: Todo;
  @Input() "i": number
@Output() todoDelete: EventEmitter<Todo>= new EventEmitter(); //1-50
@Output() todoCheckbox: EventEmitter<Todo>= new EventEmitter(); //1-50


  constructor() { }

  ngOnInit(): void {
  }

  onClick(todo: Todo){ 
    this.todoDelete.emit(todo);
    console.log("onclick triggered");
    
   }

//  onCheckboxClick(todo: Todo){  ////1-28-9

//   }

onCheckboxClick(todo: Todo){  ////1-28-9
  console.log(todo);
  
  this.todoCheckbox.emit(todo);
  console.log(todo);

}      

}
