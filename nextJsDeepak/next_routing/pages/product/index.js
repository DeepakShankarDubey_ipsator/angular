import React from "react";

import Link from "next/link";

export default function (  {productid  = 100} ) {
  return (
    <div>
      <Link href="/">Home</Link>

      <h2>
        {" "}
        <Link href="/product/1">product 1</Link>{" "}
      </h2>
      <h2>
        {" "}
        <Link href="/product/2">product 2</Link>{" "}
      </h2>
      <h2>
        {" "}
        <Link href="/product/3" replace >product 3</Link>{" "}
      </h2>
    
      <h2>
        {" "}
        <Link href= {`/product/${productid} `} >product {productid} </Link>{" "}
      </h2>

   
    </div>
  );
}
