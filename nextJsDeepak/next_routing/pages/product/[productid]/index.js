import React from 'react'

import {useRouter} from 'next/router'

export default function product_detail() {
    const router = useRouter()
    const productid= router.query.productid
  return (
    <div>product_detail of {productid} </div>
  )
}
