import Link from "next/link";
import { useRouter } from "next/router";

const Home = () => {
  const router = useRouter();

  const handleClick = () => {
    console.log("placing your order");
    router.push("/product");
  };

  return (
    <div>
      <h1> Home page </h1>

      <Link href="/blog">Blog</Link>

      <br></br>
      <br></br>
      <br></br>

      <Link href="/product">products</Link>

      <br></br>
      <br></br>
      <button onClick={handleClick}> Place Order </button>
    </div>
  );
};

export default Home;
